<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('rest.php');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
class Api extends Rest {
    function posting()
    {        
        if(!empty($this->_post_args)){
            foreach($this->_post_args as $x=>$y)
                $_POST[$x] = $this->_xss_clean ($y,true);
        }
    }
    
    function login_post($id = ''){        
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('user','Usuario','required');        
        if($this->form_validation->run())
        {
            $user = $this->db->get_where('jugadores',array('email'=>$_POST['email']));
            if($user->num_rows>0){
                if($user->row()->user==$this->input->post('user') && $user->row()->status==1)
                $this->response(array('status'=>'success','id'=>$user->row()->id,'nombre'=>$user->row()->nombre));
                elseif($user->row()->status==0)
                $this->response(array('status'=>'blocked'));
                else
                $this->response(array('status'=>'fail'));
            }
            else
            $this->response(array('status'=>'noexist'));
        }
        $this->response(array('status'=>'fail','msj'=>$this->form_validation->error_string()));
    }
    
    function registrar_post(){        
        $this->form_validation->set_rules('user','Usuario','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('nombre','Nombre','required');
        if($this->form_validation->run())
        {
            date_default_timezone_set( 'Europe/Madrid' );
            $this->db->insert('jugadores',array('user'=>$_POST['user'],'email'=>$_POST['email'],'nombre'=>$_POST['nombre'],'fecha'=>date("Y-m-d")));
            $this->login_post($this->db->insert_id());
        }
    }
    
    function init_game_post(){
        if(isset($_POST['nombre']) || isset($_POST['email']) || isset($_POST['aleatorio'])){
            if(!empty($_POST['nombre'])){
                $this->db->like('nombre',$_POST['nombre']);
                $this->db->where('jugando',0);
                $this->db->where('conectado',1);
                $this->db->limit(5);
                $data = array();
                foreach($this->db->get('jugadores')->result() as $j){
                    $data[$j->id] = array('id'=>$j->id,'nombre'=>$j->nombre);
                }
                $this->response(array('status'=>'success','jugadores'=>$data,'solicitud'=>'nombre'));
            }
            elseif(!empty($_POST['email'])){
                $this->db->where('email',$_POST['email']);
                $this->db->where('jugando',0);
                $this->db->where('conectado',1);
                $this->db->limit(5);
                $data = array();
                foreach($this->db->get('jugadores')->result() as $j){
                    $data[$j->id] = array('id'=>$j->id,'nombre'=>$j->nombre);
                }
                $this->response(array('status'=>'success','jugadores'=>$data,'solicitud'=>'email'));
            }
            elseif(!empty($_POST['aleatorio'])){
                
            }
            else
                $this->response (array('status'=>'fail','msj'=>'No se selecciono una opción'));
        }
    }
    
    function uploadSelfie_post(){
        $str="data:image/png;base64,"; 
        $data=str_replace($str,"",$_POST['img']); 
        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        file_put_contents('selfies/'.$_POST['id'].'.png', $data);
    }        
    
    function get_perfil_post(){
        $this->form_validation->set_rules('id','ID','required|greather_than[0]');
        if($this->form_validation->run()){  
            $this->db->select('jugadores.*, paises.continente, paises.nombre as paisname');
            $this->db->join('paises','paises.id = jugadores.pais','left');
            $user = $this->db->get_where('jugadores',array('jugadores.id'=>$this->input->post('id')));
            if($user->num_rows>0){
                $this->db->order_by('puntos','DESC');
                $users = $this->db->get('jugadores');
                $posicion = 0;
                foreach($users->result() as $u){
                    $posicion++;
                    if($u->id == $user->row()->id)
                        $user->row()->posicion = $posicion;
                }
                if($user->row()->pais!='' && is_numeric($user->row()->pais)){
                    $posicion = 0;
                    foreach($users->result() as $u){
                        $posicion++;
                        $this->db->select('jugadores.*, paises.continente');
                        $this->db->join('paises','paises.id = jugadores.pais');
                        $this->db->order_by('puntos','DESC');                        
                        $users = $this->db->get_where('jugadores',array('continente'=>$user->row()->continente));
                        if($u->id == $user->row()->id)
                            $user->row()->continental = $posicion;
                    }
                    
                    $posicion = 0;
                    foreach($users->result() as $u){
                        $posicion++;
                        $this->db->select('jugadores.*, paises.continente');
                        $this->db->join('paises','paises.id = jugadores.pais');
                        $this->db->order_by('puntos','DESC');                        
                        $users = $this->db->get_where('jugadores',array('paises.id'=>$user->row()->pais));
                        if($u->id == $user->row()->id)
                            $user->row()->regional = $posicion;
                    }
                }
                else{
                    $user->row()->continental = $user->row()->posicion;
                    $user->row()->regional = $user->row()->posicion;
                }
                $user->row()->foto = empty($user->row()->foto)?'img/vacio.png':base_url('pictures/'.$user->row()->foto);                
                $this->response(array('status'=>'success','data'=>$user->row()));
            }
            else
                $this->response(array('status'=>'fail'));
        }
        else $this->response(array('status'=>'fail'));
    }
    
    function set_perfil_post(){
        $this->form_validation->set_rules('id','ID','required|greather_than[0]');
        $this->form_validation->set_rules('nombre','Nombre','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');        
        if($this->form_validation->run()){
            $data = array(
                'pais'=>$this->input->post('pais'),
                'edad'=>$this->input->post('edad'),
                'nombre'=>$this->input->post('nombre'),
                'email'=>$this->input->post('email'),
                'sexo'=>$this->input->post('genero'));
            
            if(!empty($_FILES['foto']['tmp_name']))
                $data['foto'] = $this->input->post('id').'.png';
            
            
            $this->db->update('jugadores',$data, array('id'=>$this->input->post('id')));
            
            if(!empty($_FILES['foto']['tmp_name'])){
                if(file_exists('pictures/'.$this->input->post('id').'.png'))
                        unlink('pictures/'.$this->input->post('id').'.png');
                move_uploaded_file($_FILES['foto']['tmp_name'],'pictures/'.$this->input->post('id').'.png');
            }
            $this->response(array('status'=>'success'));
        }
    }
    
    function get_page_post(){
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run())
        {
            $contenido = $this->db->get_where('paginas',array('id'=>$this->input->post('id')));
            if($contenido->num_rows>0)
            $this->response(array('status'=>'success','page'=>$contenido->row()->contenido));
        }
    }
    
    function get_store_post(){
        $this->db->order_by('id','DESC');
        $store = $this->db->get_where('store',array('categoria'=>1));
        $data = array();
        foreach($store->result() as $s)
            array_push($data,array('titulo'=>$s->titulo,'categoria'=>$s->categoria,'precio'=>$s->precio));
        $this->response(array('status'=>'success','list'=>$data));
    }
    
    /********************** Mono player ****************************/
    function uploadSelfieMono_post(){
        if(isset($_POST['id'])){
            $str="data:image/png;base64,"; 
            $data=str_replace($str,"",$_POST['img']); 
            $data = str_replace(" ","+",$data);
            $data = base64_decode($data);
            $name_selfie = $_POST['id'].'_'.rand(0,2048).'.png';
            file_put_contents('pictures_alone/'.$name_selfie, $data);            
            $jugador = $this->db->get_where('monoplayer',array('jugador'=>$_POST['id']));
            /*if($jugador->num_rows>0)
            $this->db->delete('monoplayer',array('jugador'=>$_POST['id']));*/
            $this->db->insert('monoplayer',array('jugador'=>$_POST['id'],'avatar'=>$_POST['avatar'],'selfie'=>$name_selfie));
        }
    }
    
    function getsalfiemono_post(){
        if(isset($_POST['id'])){            
            $datos = $this->db->get_where('monoplayer',array('jugador'=>$_POST['id']));
            if($datos->num_rows==-1)
                $this->response(array('status'=>'success','datos'=>$datos->row()));
            else {
                $cantidad = $this->db->get('avatars');
                if($cantidad->num_rows>0){
                    $avatar = $cantidad->row(rand(0,$cantidad->num_rows-1));
                    $this->response(array('status'=>'fail','avatar'=>$avatar->archivo,'avatarname'=>$avatar->nombre));
                }
            }
        }
    }
    
    function takeotherselfie_post(){
        if(isset($_POST['id'])){            
            $this->db->delete('monoplayer',array('jugador'=>$_POST['id']));
            $this->response(array('status'=>'success'));            
        }
    }
    
    function rankselfiemono_post(){
        if(isset($_POST['id'])){                        
            $this->db->select('monoplayer.*, jugadores.nombre');
            $this->db->join('jugadores','jugadores.id = monoplayer.jugador');
            $this->db->where('jugador != ',$_POST['id']);            
            $this->db->where('rankeador',0);
            $self = $this->db->get('monoplayer');
            if($self->num_rows>0){
                $self->row()->avatarname = $this->db->get_where('avatars',array('archivo'=>$self->row()->avatar))->row()->nombre;
                $this->response(array('status'=>'success','datos'=>$self->row()));
            }
            else $this->response(array('status'=>'fail'));
        }
    }
    
    function setRankMono_post(){
        $this->form_validation->set_rules('jugador','ID','required|greather_than[0]');
        $this->form_validation->set_rules('rankeador','Nombre','required|greather_than[0]');
        $this->form_validation->set_rules('ranking','Email','required');
        $this->form_validation->set_rules('id','ID','required');
        if($this->form_validation->run()){
            $data = array();
            $data['rankeador'] = $_POST['rankeador'];
            $data['status'] = 1;
            $data['ojos'] = $_POST['ranking'][0];
            $data['cabello'] = $_POST['ranking'][1];
            $data['nariz'] = $_POST['ranking'][2];
            $data['boca'] = $_POST['ranking'][3];
            $data['atuendo'] = $_POST['ranking'][4];
            $data['puntos'] = 0;
            foreach($_POST['ranking'] as $p)
                $data['puntos']+=$p;      
            $selfie = 'pictures_alone/'.$this->db->get_where('monoplayer',array('id'=>$_POST['id']))->row()->selfie;
            if(file_exists($selfie))unlink($selfie);
            $this->db->delete('monoplayer',array('id'=>$_POST['id']));
            
            $puntos = $data['puntos']+$this->db->get_where('jugadores',array('id'=>$_POST['jugador']))->row()->puntos;
            $monedas = $this->db->get_where('jugadores',array('id'=>$_POST['rankeador']))->row()->monedas+1;
            $this->db->update('jugadores',array('puntos'=>$puntos),array('id'=>$_POST['jugador']));
            $this->db->update('jugadores',array('monedas'=>$monedas),array('id'=>$_POST['rankeador']));
            $this->response(array('status'=>'success'));
        }
        else $this->response(array('status'=>'fail'));
    }
    
    function restpoints_post(){
        $this->form_validation->set_rules('jugador','ID','required|greather_than[0]');
        $this->form_validation->set_rules('cantidad','Cantidad','required|greather_than[0]');
        if($this->form_validation->run()){
            $monedas = $this->db->get_where('jugadores',array('id'=>$_POST['jugador']));            
            if($monedas->num_rows>0){
                $monedas = $monedas->row()->monedas;
                $cantidad = $monedas-$_POST['cantidad'];
                if($cantidad>=0){
                    $this->db->update('jugadores',array('monedas'=>$cantidad),array('id'=>$_POST['jugador']));
                    $this->response(array('status'=>'success'));
                }
                else $this->response(array('status'=>'fail'));
            }
            else $this->response(array('status'=>'fail'));
        }
        else $this->response(array('status'=>'fail'));
    }
    
    function get_avatars_category_post(){
        $avatars = $this->db->get('paquetes_avatares');
        $avatar = array();
        foreach($avatars->result() as $a){
            $avatar[$a->id] = array('nombre'=>$a->nombre,'precio'=>$a->precio);
        }
        
        $this->response(array('status'=>'success','avatars'=>$avatar));
        
    }
    
    function get_avatars_post(){
        $this->form_validation->set_rules('id','ID','required|greather_than[0]');        
        if($this->form_validation->run()){
            $array = array();
            foreach($this->db->get_where('avatars',array('paquete'=>$this->input->post('id')))->result() as $a){
                $array[] = array($a->archivo,$a->nombre);
            }           
            $this->response(array('status'=>'success','avatars'=>$array));
        }
        else $this->response(array('status'=>'fail'));
        
    }
}