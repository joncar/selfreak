<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Panel extends Main {
        
	public function __construct()
	{
		parent::__construct();
                if(empty($_SESSION['user']))
                header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
                $this->load->library('html2pdf/html2pdf');
                $this->load->library('image_crud');
                ini_set('date.timezone', 'America/Caracas');
                date_default_timezone_set('America/Caracas');                
	}
       
        public function index($url = 'main',$page = 0)
	{		
                $this->loadView('panel');
	}                
        
        public function loadView($crud)
        {
            if(empty($_SESSION['user']))
            header("Location:".base_url('index.php?redirect='.str_replace("/sgv/","",$_SERVER['REQUEST_URI'])));
            else
            parent::loadView($crud);
        }
        
        function usuarios($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields()
                 ->unset_columns('password');
            //Displays
            $crud->display_as('password','Contraseña');            
            //Fields types            
            //Validations  
            $crud->required_fields(
                    'nombre','apellido','email',
                    'password','password2',
                    'pais','ciudad','tipodocumento',
                    'lugarnac','sexo','nacionalidad',
                    'acceso_mayorista',
                    'direccion','nrocelular','password','usuario','cedula','rut','admin','status');  
            if(empty($y))
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            $crud->set_rules('usuario','Usuario','required|is_unique[user.usuario]');
            $crud->set_rules('password','Contraseña','required|min_length[8]');                                
            $crud->field_type('status','true_false',array('0'=>'Bloqueado','1'=>'Activo'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));            
            //Callbacks            
            $crud->callback_before_insert(array($this,'usuario_binsertion'));
            $crud->callback_after_insert(array($this,'usuario_ainsertion'));
            $crud->callback_before_update(array($this,'usuario_binsertion'));
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de clientes';
            $output->menu = 'usuarios';
            $this->loadView($output);
        }
        
        function tipoavatars(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('tipoavatars');
            $crud->set_subject('Tipo');
            //Fields            
            //unsets
            //Displays       
            $crud->required_fields('nombre');     
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de partes';
            $output->menu = 'tipoavatars';
            $this->loadView($output);
        }
        
        function preguntas(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('preguntas');
            $crud->set_subject('Pregunta');
            //Fields            
            //unsets
            //Displays       
            $crud->required_fields('nombre');     
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de preguntas';
            $output->menu = 'preguntas';
            $this->loadView($output);
        }
        
        function avatars($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('avatars');
            $crud->set_subject('Avatar');               
            $crud->set_relation('tipoavatar','tipoavatars','nombre');
            $crud->set_relation('paquete','paquetes_avatares','nombre');
            $crud->required_fields('tipoavatar','nombre','archivo');
            $crud->set_field_upload('archivo','avatars');
            //Fields            
            //unsets
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de avatars';
            $output->menu = 'avatars';
            $this->loadView($output);
        }
        
        function jugadores($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('jugadores');
            $crud->set_subject('Jugadores');
            $crud->field_type('jugando','true_false',array(0=>'No','1'=>'Si'));
            $crud->field_type('conectado','true_false',array(0=>'No','1'=>'Si'));
            $crud->unset_add()                 
                 ->unset_delete();
            $crud->unset_columns('token');
            $crud->display_as('fecha','Fecha registro');
            //Fields            
            //unsets
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de jugadores';
            $output->menu = 'jugadores';
            $this->loadView($output);
        }
        
        function paginas($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('paginas');
            $crud->set_subject('Paginas');
            $crud->required_fields('titulo');
            $crud->unset_edit_fields('titulo');
            $crud->unset_delete();
            $crud->unset_columns('token');
            $crud->display_as('fecha','Fecha registro');
            //Fields            
            //unsets
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de paginas';
            $output->menu = 'paginas';
            $this->loadView($output);
        }
        
        function categorias($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('categorias');
            $crud->set_subject('Categorias');
            $crud->required_fields('nombre');
            $crud->unset_delete();
            //Fields            
            //unsets
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de categorias del store';
            $output->menu = 'categorias';
            $this->loadView($output);
        }
        
        function store($x = '', $y = '')
        {
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('store');
            $crud->set_subject('Productos');
            $crud->set_relation('categoria','categorias','nombre');
            $crud->required_fields('titulo','categoria','producto');
            $crud->unset_delete();
            //Fields            
            //unsets
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Registro de productos del store';
            $output->menu = 'productos';
            $this->loadView($output);
        }
        
        function app()
        {            
            $this->loadView(array('view'=>'mobile','menu'=>'app'));
        }
        
        function usuario_binsertion($post,$primary = '')
        {                        
            if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary))
                $post['password'] = md5($post['password']);
            return $post;
        }
        
        function paquetes(){
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('paquetes_avatares');
            $crud->set_subject('Paquetes');            
            $crud->required_fields('nombre','precio');
            $crud->unset_delete();
            //Fields            
            //unsets
            //Displays            
            //Fields types            
            //Validations              
            $output = $crud->render();
            $output->view = 'panel';
            $output->crud = 'user';
            $output->title= 'Paquetes de avatares';
            $output->menu = 'paquete';
            $this->loadView($output);
        }
        /*Cruds*/              
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */