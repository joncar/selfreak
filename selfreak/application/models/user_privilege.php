<?php
class User_privilege  extends grocery_CRUD_Model  {

	function get_list()
        {
    	if($this->table_name === null)
    		return false;

    	$select = "`{$this->table_name}`.*";

    	//set_relation special queries
    	if(!empty($this->relation))
    	{
    		foreach($this->relation as $relation)
    		{
    			list($field_name , $related_table , $related_field_title) = $relation;
    			$unique_join_name = $this->_unique_join_name($field_name);
    			$unique_field_name = $this->_unique_field_name($field_name);

				if(strstr($related_field_title,'{'))
				{
					$related_field_title = str_replace(" ","&nbsp;",$related_field_title);
    				$select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $unique_field_name";
				}
    			else
    			{
    				$select .= ", $unique_join_name.$related_field_title AS $unique_field_name";
    			}

    			if($this->field_exists($related_field_title))
    				$select .= ", `{$this->table_name}`.$related_field_title AS '{$this->table_name}.$related_field_title'";
    		}
    	}

    	//set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
    	if(!empty($this->relation_n_n))
    	{
			$select = $this->relation_n_n_queries($select);
    	}          
    	$this->db->select($select, false);        
    	$results = $this->db->get($this->table_name)->result();
    	return $results;
        }
        
        function db_update($post_array, $primary_key_value)
        {
            $primary_key_field = $this->get_primary_key();
            return $this->db->update($this->table_name,$post_array, array( $primary_key_field => $primary_key_value,'user'=>$_SESSION['user']));
        }

        function db_insert($post_array)
        {
            $post_array['user'] = $_SESSION['user'];
            $insert = $this->db->insert($this->table_name,$post_array);
            if($insert)
            {
                    return $this->db->insert_id();
            }
            return false;
        }

        function db_delete($primary_key_value)
        {
            $primary_key_field = $this->get_primary_key();

            if($primary_key_field === false)
                    return false;

            $this->db->limit(1);
            $this->db->delete($this->table_name,array( $primary_key_field => $primary_key_value,'user'=>$_SESSION['user']));
            if( $this->db->affected_rows() != 1)
                    return false;
            else
                    return true;
        }
        
        function get_edit_values($primary_key_value)
        {
            $field = $this->table_name=='user'?'user.id':'user';
            $this->db->where($field,$_SESSION['user']);            
            $primary_key_field = $this->get_primary_key();
            $this->db->where($primary_key_field,$primary_key_value);
            $result = $this->db->get($this->table_name)->row();
            return $result;
        }
}
?>
