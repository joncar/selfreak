<? $this->load->view('includes/nav') ?>
<section class="row" style="margin-right: 0px; margin-right:0px;">
    <div class="col-sm-12 col-xs-12">
        <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
            <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
            <?= !empty($msj)?$msj:'' ?>
            <?= input('email','Email','email') ?>
            <?= input('cedula','Cedula','cedula') ?>
            <div class="form-group">
              <label for="email" class="col-sm-4 control-label">Carrera</label>
               <div class="col-sm-8">
                <?php $data = array(); ?>
                <?php foreach($this->db->get('Carreras')->result() as $x)
                      $data[$x->Cod_Carrera] = $x->Nombre ?>
                <?= form_dropdown('carrera',$data,0,'id="field-carrera" class="form-control"') ?>
                </div>
             </div>            
            <div align='center'><button type="submit" class="btn btn-success">Recuperar pin</button></div>
        </form>
    </div>    
</section>