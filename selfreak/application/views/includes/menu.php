<div class="col-sm-3 col-xs-12">
    <div class="list-group">            
        <a href="<?= base_url('panel'); ?>" class="<?= empty($menu) || $menu=='panel'?'active':'' ?> list-group-item">Resumenes</a>                    
        <a href="<?= base_url('panel/usuarios'); ?>" class="<?= !empty($menu) && $menu=='usuarios'?'active':'' ?> list-group-item">Usuarios</a>
        <a href="<?= base_url('panel/tipoavatars'); ?>" class="<?= !empty($menu) && $menu=='tipoavatars'?'active':'' ?> list-group-item">Partes</a>
        <a href="<?= base_url('panel/paquetes'); ?>" class="<?= !empty($menu) && $menu=='paquete'?'active':'' ?> list-group-item">Paquetes de avatares</a>
        <a href="<?= base_url('panel/avatars'); ?>" class="<?= !empty($menu) && $menu=='avatars'?'active':'' ?> list-group-item">Avatares</a>
        <a href="<?= base_url('panel/preguntas'); ?>" class="<?= !empty($menu) && $menu=='preguntas'?'active':'' ?> list-group-item">Preguntas</a>
        <a href="<?= base_url('panel/jugadores'); ?>" class="<?= !empty($menu) && $menu=='jugadores'?'active':'' ?> list-group-item">Jugadores</a>
        <a href="<?= base_url('panel/paginas'); ?>" class="<?= !empty($menu) && $menu=='paginas'?'active':'' ?> list-group-item">Paginas</a>
        <a href="<?= base_url('panel/categorias'); ?>" class="<?= !empty($menu) && $menu=='categorias'?'active':'' ?> list-group-item">Categorias</a>
        <a href="<?= base_url('panel/store'); ?>" class="<?= !empty($menu) && $menu=='productos'?'active':'' ?> list-group-item">Store</a>
        <a href="<?= base_url('panel/app'); ?>" class="<?= !empty($menu) && $menu=='app'?'active':'' ?> list-group-item">Maquetador de app</a>
    </div>
</div>