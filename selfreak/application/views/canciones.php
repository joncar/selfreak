<? $this->load->view('includes/nav') ?>
<section class="row" style="margin-right: 0px; margin-right:0px;">
    <?= $this->load->view('includes/menu') ?>    
    <div class="col-sm-9 col-xs-12" style="padding:20px; background:#f1fafa;">            
        <input type="file" multiple="multiple" id="upload_field" />
        <input type="hidden" name="disco" id="disco" value="<?= $disco ?>">
        <input type="hidden" name="genero" id="genero" value="<?= $genero ?>">
        <div id="progress_report">
            <div id="progress_report_name"></div>
            <div id="progress_report_status" style="font-style: italic;"></div>
            <div id="progress_report_bar_container" style="width: 90%; height: 5px;">
                <div id="progress_report_bar" style="background-color: blue; width: 0; height: 100%;"></div>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $("#upload_field").html5_upload({
                    url: function(number) {
                        return "/panel/upload_file";
                    },
                    sendBoundary: window.FormData || $.browser.mozilla,
                    onStart: function(event, total) {
                        return true;
                        return confirm("You are trying to upload " + total + " files. Are you sure?");
                    },
                    onProgress: function(event, progress, name, number, total) {
                        console.log(progress, number);
                    },
                    setName: function(text) {
                        $("#progress_report_name").text(text);
                    },
                    setStatus: function(text) {
                        $("#progress_report_status").text(text);
                    },
                    setProgress: function(val) {
                        $("#progress_report_bar").css('width', Math.ceil(val*100)+"%");
                    },
                    onFinishOne: function(event, response, name, number, total) {
                        //alert(response);
                    },
                    onError: function(event, name, error) {
                        alert('error while uploading file ' + name);
                    }
                });
            });
        </script>
        </div>
</section>