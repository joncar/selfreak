var WebSocketServer = require('websocket').server;
var mysql = require('./mysql');
var path = 'http://217.160.131.136/selfreak/';
mysql.unlogall();
mysql.finishgameall();
var http = require('http');
var server = http.createServer(function(request,response){});
server.listen(3000);
var au = '';
wsServer = new WebSocketServer({
    httpServer:server
});

var salas = [], jugadores, path_selfie = '../selfies/';

function aleatorio(inferior,superior)
{
      var numPosibilidades = superior - inferior;
      var aleat = Math.random() * numPosibilidades;
      aleat = Math.round(aleat);            
      return parseInt(inferior) + aleat;
}

JugadoresObject = function(){
    this.list = [];
    
    this.add = function(jugador){
        this.list.push(jugador);
    }
    
    this.remove = function(jugador){
        if(jugador!=undefined){
            aux = [];
            for(i in this.list){
                if(this.list[i].id!=jugador.id)
                    aux.push(this.list[i]);
            }
            this.list = aux;
        }
    }
    
    this.update = function(jugador){
        for(i in this.list){
            if(this.list[i].id==jugador.id)
                this.list[i] = jugador;
        }
    }
    
    this.getFromId = function(id){
        jug = null;
        for(i in this.list){
            if(this.list[i].id==id)
                jug = this.list[i];
        }
        return jug;
    }    
}

SalasObject = function(){
    this.list = [];
    
    this.add = function(sala){
        this.list.push(sala);
    }
    
    this.remove = function(sala){        
        aux = [];
        for(i in this.list){
            if(this.list[i].id!=sala.id)
                aux.push(this.list[i]);
        }
        this.list = aux;
    }
    
    this.update = function(sala){
        for(i in this.list){
            if(this.list[i].id==sala.id)
                this.list[i] = sala;
        }
    }
    
    this.getFromId = function(id){
        sal = null;
        for(i in this.list){
            if(this.list[i].id==id)
                sal = this.list[i];
        }
        return sal;
    }
}

salas = new SalasObject();
jugadores = new JugadoresObject();

function sala(){
    do{
        this.id = aleatorio(0,1024);
    }while(salas.getFromId(this.id)!=null);    
    this.jugadores = [];
    this.tiempo_espera_jugador = 10; //Cuanto tiempo se esperara por nuevos jugadores
    this.tiempo_espera_empezar = 10; //Cuanto tiempo esperara para comenzar el juego
    this.tiempo_espera_aleatorio = 10; //Cuanto se crea una partida en modo aleatorio y se espera otro jugador para iniciar
    this.max_rondas = 3; //Cuantas rondas maximas por juego
    this.max_participantes = 5; //Maximo de jugadores permitidos por sala
    this.tiempo_init_juego = 0; //Cuanto tiempo iniciara el reloj de los jugadores
    this.param_reloj; //Actualización del reloj
    this.status = 0;
    this.ranking_group = [];
    this.ronda = 0;
    this.newJugadorJoined = false;
}
//Iniciar la sala al momento de que se aceptan las invitaciones
sala.prototype.init_sala = function(invitante,contrincante){
    this.status = 0;            
    this.param_reloj = this.tiempo_espera_jugador;
    this.jugadores = new JugadoresObject();  
    mysql.initgame(invitante.id);
    mysql.initgame(contrincante.id);
    this.jugadores.add(this.init_jugador_values(invitante));
    this.jugadores.add(this.init_jugador_values(contrincante));    
    
    //Envia el reloj a los jugadores
    this.broadcast('wait_for_players',{time:this.param_reloj});
    //Envia lista de jugadores
    this.refreshlist();
}

sala.prototype.join_jugador = function(jug){
    if(this.status>=0)
        this.newJugadorJoined = true;
    mysql.initgame(jug.id);
    this.jugadores.add(this.init_jugador_values(jug));
}

sala.prototype.init_jugador_values = function(jug){
    jug.puntos = 0;
    jug = this.cleanValues(jug);
    return jug;
}

sala.prototype.cleanValues = function(jug){
    jug.selfie = '';
    jug.avatar = '';
    jug.tocarankear = -1; //A este jugador ya le rankearon
    jug.ranking = []; //ojos,cabello,nariz,boca,atuendo
    jug.time = 0;
    return jug;
}
//Traer un arreglo con todos los datos de los jugadores
sala.prototype.get_jugadores = function(){
    jug = [];
    for(i in this.jugadores.list){
        juga = this.jugadores.list[i];
        //Verificar si ya envio el selfie para que rankee a el jugador siguiente
        if(juga.selfie!='')
        {
            tocarankear = parseInt(i)+1;
            tocarankear = this.jugadores.list.length>tocarankear?tocarankear:0;
            //Si el jugador a rankear se tomo el selfie y aun no ha sido rankeado
            ranking = this.jugadores.list[tocarankear].ranking;            
            if(this.jugadores.list[tocarankear].selfie!='' && ranking.length==0)
                juga.tocarankear = tocarankear;
            else 
                juga.tocarankear = -1;
        }
        ran = this.ranking_group;  
        param = this.param_reloj-juga.time;
        rank = [];
        for(i in ran){
            rank.push({nombre:ran[i].nombre,puntos:ran[i].puntos,id:ran[i].id,foto:ran[i].foto});
        }
        jug.push({foto:juga.foto,id:juga.id,avatar:juga.avatar,nombre:juga.nombre,puntos:juga.puntos,selfie:juga.selfie,avatarname:juga.avatarname,time:param,monedas:juga.monedas,tocarankear:juga.tocarankear,ranking:juga.ranking,ranking_group:rank});
    }    
    return jug;
}
//Sacar a un jugador de la sala
sala.prototype.out_jugador = function(id){
    jug = this.jugadores.getFromId(id);
    if(jug!=null){
        this.jugadores.remove(jug);
        mysql.finishgame(jug.id);
        this.refreshlist();
        
        if(this.jugadores.list.length<=1){//Si resulta que solo queda un participante o todos se fueron, se acaba el juego
            if(this.jugadores.list.length==1){
                for(i in this.jugadores.list)
                mysql.finishgame(this.jugadores.list[i].id)
            }
            this.finish_game();
        }
    }
}
//Actualizaciones de status
sala.prototype.play_game_init = function(){
    this.param_reloj = this.tiempo_espera_empezar;
    this.status = 1;
    //Envia el reloj a los jugadores
    this.broadcast('wait_for_init_play',{time:this.param_reloj});
}
//Inicia el juego
sala.prototype.play_game = function(){
    this.param_reloj = 0;
    this.status = 2;
    this.ronda = this.ronda==0?1:this.ronda;
    //Envia el reloj a los jugadores
    this.ranking_group = this.jugadores.list;
    this.broadcast('play',{time:this.param_reloj,ronda:this.ronda});
    au = this;
    //Repartir selfies
    mysql.query('SELECT * from avatars',null,function(error, result){
      if(error){
            throw error;
            console.log(error);
         }else{
            cantidad = result.length;
            for(i in au.jugadores.list)
            {
                avatar = result[aleatorio(0,cantidad-1)];
                au.jugadores.list[i].avatar = avatar.archivo;                
                au.jugadores.list[i].avatarname = avatar.nombre;
            }
            au.refreshlist();
         }
      }
    );    
}
//Finalizar la sala
sala.prototype.finish_game = function(){    
    if(this.status==-1){
        this.broadcast('finish_game',{msj:'No se han encontrado partidas para jugar, intentelo de nuevo'});
    }
    else if(this.status==2 && this.ronda > this.max_rondas){
        this.broadcast('finish_game',{msj:'Juego terminado'});
    }
    else if(this.jugadores.list.length<=1){//Si se desconectan todos
        this.broadcast('finish_game',{msj:'Todos los jugadores se han desconectado'});
    }
    
    ran = this.ranking_group;       
    for(i in ran){        
        mysql.update_puntos(ran[i].id,ran[i].puntos);
    }
    
    this.status=3;
}

//Refrescar las listas de la sala
sala.prototype.refreshlist = function(){    
    this.ranking_group_order();
    jug = this.get_jugadores();
    this.broadcast('refreshlistplayer',{players:jug});    
}
//Enviar un sms a todos los integrantes de la sala
sala.prototype.broadcast = function(evt,data){
    for(i in this.jugadores.list){
        this.jugadores.list[i].sendUTF(JSON.stringify({'evt':evt,data:data}));
    }
}

//Cambiar de ronda
sala.prototype.changeronda = function(){
    rankeado = 0;    
    for(i in this.jugadores.list){
        if(this.jugadores.list[i].ranking.length!=0)
            rankeado++;
    }    
    if(rankeado==this.jugadores.list.length){//Si todos fueron rankeados
        this.ronda++;
        if(this.status==2 && this.ronda > this.max_rondas)
            this.finish_game();
        if(this.status==2){
            for(i in this.jugadores.list)
                this.jugadores.list[i] = this.cleanValues(this.jugadores.list[i]);
            this.play_game();
            this.broadcast('changeronda',{data:''});
            this.refreshlist();
        }
    }
}
//Ordenar el ranking de mayor puntuacion a menor
sala.prototype.ranking_group_order = function(){
    aux = [];
    ran = this.ranking_group;
    for(i in ran){
        for(k in ran){
            if(ran[i].puntos>ran[k].puntos){
                aux = ran[i];
                ran[i] = ran[k];
                ran[k] = aux;
            }
        }
    }
    this.ranking_group = ran;
}

wsServer.on('request',function(request){
    var jugador = request.accept(null,request.origin);
    jugador.id, jugador.nombre, jugador.avatar = '', jugador.selfie = '', jugador.tiempo_selfie = 0, jugador.puntos = 0, jugador.rank = 0, jugador.time = 0;
    //Eventos
    jugador.on('message',function(message){        
        datafromclient = JSON.parse(message.utf8Data);        
        switch(datafromclient.evt){
            case 'login': jugador.login(datafromclient.data); break;
            case 'send_invitation': jugador.send_invitation(datafromclient.data); break;
            case 'create_sala': jugador.createSala(datafromclient.data); break;
            case 'acceptinvitation': jugador.acceptinvitation(datafromclient.data); break;
            case 'closematch': jugador.closematch(); break;
            case 'sendphoto': jugador.sendphoto(datafromclient.data); break;
            case 'aleatorio': jugador.aleatorio(datafromclient.data); break;
            case 'sendRank': jugador.rankear(datafromclient.data); break;
            case 'change_avatar': jugador.change_avatar(datafromclient.data); break;
            case 'getmoretime': jugador.getmoretime(); break;
        }    
        
    });
    
    jugador.on('close', function(reasonCode, description) {        
        //Cuando se desconecta el invitado y tenia una invitacion activa se rechaza
        if(jugador.invitante!=undefined && jugador.sala==undefined){
            jugador.acceptinvitation({respuesta:false});
            jugador.invitante = undefined;
        }
        //Cuando se desconecta el invitante y tenia una invitacion activa se anula.
        else if(jugador.contrincante!=undefined && jugador.sala==undefined)
        {
            jugador.invitante = jugador.contrincante;
            jugador.acceptinvitation({respuesta:false});
            jugador.invitante = undefined;
            jugador.contrincante = undefined;
        }
        mysql.unlog(jugador.id);
        if(jugador.id!=undefined){
            jugadores.remove(jugador);//Desconectar de la lista
            //Desconectar por si estaba en una sala
            for(i in salas.list){
                salas.list[i].out_jugador(jugador.id);                
            }
            
        }
    });
    
    jugador.login = function(data){        
        jugador.id = data.id;
        jugador.nombre = data.nombre;
        mysql.login(jugador.id);
        mysql.get_data(jugador.id,function(err,result){
            jugador.monedas = parseInt(result[0].monedas);
            jugador.foto = result[0].foto==''?'img/vacio.png':path+'pictures/'+result[0].foto;
        });
        jugadores.add(jugador);
    };
    //Jugador envia la invitacion
    jugador.send_invitation = function(data){
        if(jugador.id!=data.contrincante){
            contrincante = jugadores.getFromId(data.contrincante);
            //Si el jugador esta conectado
            if(contrincante!=null){
                contrincante.sendUTF(JSON.stringify({evt:'get_invitation',data:{player:jugador.id,playername:jugador.nombre}}));
                contrincante.invitante = jugador.id;
                jugador.contrincante = data.contrincante;
            }
            else
            {
                jugador.invitante = jugador.id;
                jugador.acceptinvitation(false);          
                jugador.contrincante=undefined;
                jugador.invitante = undefined;
            }    
        }
        //Si se invita a el mismo
        else{
            jugador.invitante = jugador.id;
            jugador.acceptinvitation(false);          
            jugador.contrincante=undefined;
            jugador.invitante = undefined;
        }
    }
    //Jugador acepta una invitacion
    jugador.acceptinvitation = function(data){
        invitante = jugadores.getFromId(jugador.invitante);
        invitante.sendUTF(JSON.stringify({evt:'answer_invitation',data:{respuesta:data.respuesta}}));
        if(data.respuesta){
            jugador.createSala(invitante,jugador);            
            jugador.contrincante = undefined;
            jugador.invitante = undefined;
            invitante.contrincante = undefined;
            invitante.invitante = undefined;
        }
    }
    //Se crea la sala
    jugador.createSala = function(invitante,contrincante){
        sal = new sala();
        sal.init_sala(invitante,contrincante);
        salas.add(sal);
    }
    //Jugador abandona la sala
    jugador.closematch = function(){
        for(i in salas.list){
            salas.list[i].out_jugador(jugador.id);                
        }
    }
    //Jugador envia una foto
    jugador.sendphoto = function(img){
        jugador.selfie = 'selfies/'+jugador.id+'.png';        
        
        for(i in salas.list){
            sal = salas.list[i];
            jug = sal.jugadores.getFromId(jugador.id);
            if(jug!=null)
            {
                rel = sal.param_reloj-jug.time;
                puntos = rel<=15?30:rel<=30?20:10;
                jugador.puntos+=puntos;
                jugador.time = sal.param_reloj;
                sal.refreshlist();
            }            
        }        
    }
    
    //Cuando el jugador selecciono jugar de manera aleatoria
    jugador.aleatorio = function(){
        //buscar partidas que esperan jugadores
        partidaencontrada = false;
        for(i in salas.list){            
            if(salas.list[i].status==-1){
                salas.list[i].join_jugador(jugador);
                salas.list[i].refreshlist();
                jugador.sendUTF(JSON.stringify({evt:'answer_invitation',data:{respuesta:true}}));
                partidaencontrada = true;
            }
        }
        if(!partidaencontrada){
            for(i in salas.list){
                if(salas.list[i].status==0 && salas.list[i].length<salas.list[i].max_participantes){
                    salas.list[i].join_jugador(jugador);
                    salas.list[i].refreshlist();
                    sal = salas.list[i];
                    jugador.sendUTF(JSON.stringify({evt:'answer_invitation',data:{respuesta:true}}));                    
                    partidaencontrada = true;
                }
            }
        }
        if(!partidaencontrada){
            for(i in salas.list){
                if(salas.list[i].status==1 && salas.list[i].length<salas.list[i].max_participantes){
                    salas.list[i].join_jugador(jugador);
                    salas.list[i].refreshlist();
                    jugador.sendUTF(JSON.stringify({evt:'answer_invitation',data:{respuesta:true}}));                    
                    partidaencontrada = true;
                }
            }
        }
        if(!partidaencontrada){
            sal = new sala();
            sal.status=-1;
            sal.jugadores = new JugadoresObject();
            sal.join_jugador(jugador);            
            sal.param_reloj = sal.tiempo_espera_aleatorio;
            salas.add(sal);            
        }
    }
    
    //Rankear a el jugador
    jugador.rankear = function(ranking){
        
        for(i in salas.list){
            sal = salas.list[i];
            jug = sal.jugadores.getFromId(jugador.id);
            if(jug!=null)
            {                
                juga = sal.jugadores.list[jugador.tocarankear];
                juga.ranking = ranking;
                juga.sendUTF(JSON.stringify({'evt':'getranking',data:{ranking:ranking}}));
                jugador.tocarankear = -1;
                for(i in ranking)
                    juga.puntos+= ranking[i];
                sal.refreshlist();
            }            
        }
    }
    
    //Funcion para cambiar el avatar de un jugador
    jugador.change_avatar = function(){
        mysql.get_data(jugador.id,function(err,result){
            
            if(parseInt(result[0].monedas)-5>=0)
            {
                mysql.restar_monedas(jugador.id,5);
                for(i in salas.list){
                    sal = salas.list[i];
                    if(sal.jugadores.getFromId(jugador.id)!=null){
                        jugador.monedas-=5;
                        au = sal;
                        mysql.query('SELECT * from avatars',null,function(error, result){                            
                                cantidad = result.length;                            
                                avatar = result[aleatorio(0,cantidad-1)];
                                jugador.avatar = avatar.archivo                
                                au.refreshlist();
                            }
                          );   
                        sal.refreshlist();
                    }
                }
            }
        });        
    }
    
    jugador.getmoretime = function(){
        mysql.get_data(jugador.id,function(err,result){
            
            if(parseInt(result[0].monedas)-5>=0)
            {
                mysql.restar_monedas(jugador.id,5);
                for(i in salas.list){
                    sal = salas.list[i];
                    if(sal.jugadores.getFromId(jugador.id)!=null){
                        jugador.monedas-=5;
                        jugador.time += 5;
                        sal.refreshlist();
                    }
                }
            }
        });
    }
});

//Reloj Principal
function reloj(){
    for(i in salas.list){
        sal = salas.list[i];
        
        switch(sal.status){
            case -1:                 
                if(sal.jugadores.list.length>1){                    
                    sal.status = 1;
                    sal.broadcast('answer_invitation',{respuesta:true});
                    sal.broadcast('wait_for_players',{time:sal.param_reloj});
                    sal.param_reloj = sal.tiempo_espera_jugador;
                    //Envia lista de jugadores
                    sal.refreshlist();
                }
                else{
                    sal.param_reloj-=1;
                    if(sal.param_reloj<=0)
                    sal.finish_game();
                }
            break;
            case 0: 
                //Verificar si ingreso un nuevo jugador antes del conteo
                sal.param_reloj-=1;
                if(sal.newJugadorJoined){
                    sal.newJugadorJoined = false;
                    sal.broadcast('wait_for_players',{time:sal.param_reloj});
                }
                if(sal.param_reloj<=0){
                    sal.play_game_init();
                }
            break; //La sala espera por jugadores
            case 1:                 
                sal.param_reloj-= 1;
                //Verificar si ingreso un nuevo jugador antes del conteo
                if(sal.newJugadorJoined){
                    sal.newJugadorJoined = false;
                    sal.broadcast('wait_for_init_play',{time:sal.param_reloj});
                }
                if(sal.param_reloj<=0){
                    sal.play_game();
                }
            break; //La sala espera por empezar
            case 2:
                sal.param_reloj+= 1;
                //Verificar si ya todos rankearon
                sal.changeronda();
            break; //La sala esta en juego
            case 3:
                salas.remove(sal);                
            break; //La sala ha terminado
        }
    }
    setTimeout(reloj,1000);
}
reloj();

console.log('Servidor Iniciado');