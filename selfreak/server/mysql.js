var mysql = require('mysql');
var connection = mysql.createConnection({
   host: 'localhost',
   user: 'root',
   password: 'cP2Bn6FQ',
   database: 'selfreak',
   port: 3306
});

connection.connect(function(error){
   if(error){
      throw error;
   }else{
      console.log('Conectado con mysql');
   }
});

connection.login = function (id){
    connection.query("update jugadores set conectado='1' where id='"+id+"'");
}

connection.get_data = function(id,cb){
    connection.query("select * from jugadores where id='"+id+"'",function(err,result){
        cb(err,result);
    });    
}

connection.unlog = function (id){
    connection.query("update jugadores set conectado='0' where id='"+id+"'");    
}

connection.unlogall = function(){
    connection.query("update jugadores set conectado='0'");
}

connection.initgame = function(id){
    connection.query("update jugadores set jugando='1' where id='"+id+"'");
}

connection.finishgame = function(id){
    connection.query("update jugadores set jugando='0' where id='"+id+"'");
}

connection.finishgameall = function(){
    connection.query("update jugadores set jugando='0'");
}

connection.update_puntos = function(id,puntos){
        
    var query = connection.query('SELECT puntos from jugadores where id = ?',[id],function(error, result){
      if(error){
            throw error;
            console.log(error);
         }else{
            var resultado = result;
            if(resultado.length > 0){
               puntosTotal = parseInt(resultado[0].puntos)+puntos;
               console.log("update jugadores set puntos='"+puntosTotal+"' where id='"+id+"'");
               connection.query("update jugadores set puntos='"+puntosTotal+"' where id='"+id+"'");
            }else{
                
            }
         }
      }
    );    
}

connection.restar_monedas = function(id,cantidad){
    var query = connection.query('SELECT * from jugadores where id = ?',[id],function(error, result){
      if(error){
            throw error;
            console.log(error);
         }else{
            var resultado = result;
            if(resultado.length > 0){               
               monedas = parseInt(resultado[0].monedas)-cantidad;
               if(monedas>=0){
                    connection.query("update jugadores set monedas='"+monedas+"' where id='"+id+"'");                    
               }
            }else{
                
            }
         }
      }
    );   
}

//connection.end();

module.exports = connection;