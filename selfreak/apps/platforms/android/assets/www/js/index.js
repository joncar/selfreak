/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        initApp();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
var lista = [];
var url = 'http://app.proyectosonidero.com/api/';
var list;
var sonidero = angular.module('sonidero',['ngRoute','ngResource','angular-gestures']);
var entries = [];


sonidero.config(['$routeProvider',function($routeProvider){
        $routeProvider
                .when('/',{
                    templateUrl:'includes/main.html',  
                    controller:'main'
                })
                .when('/generos',{
                    templateUrl:'includes/generos.html',
                    controller:'generoslist'
                })
                .when('/discos/:id',{
                    templateUrl:'includes/discos.html',
                    controller:'discos'
                })
                .when('/disco/:id',{
                    templateUrl:'includes/disco.html',
                    controller:'disco'
                })
                .when('/eventos',{
                    templateUrl:'includes/eventos.html',
                    controller:'eventos'
                })
                .when('/eventos/:id',{
                    templateUrl:'includes/eventosShow.html',
                    controller:'eventosShow'
                })
                .when('/eventos_add',{
                    templateUrl:'includes/eventosAdd.html',
                    controller:'eventosAdd'
                })
                .when('/music_off',{
                    templateUrl:'includes/music_off.html',
                    controller:'music_off'
                })
                .otherwise({
                    redirectTo:'/'
                });
}]);                    

//Controllers
sonidero.controller('generoslist',['$scope','Generos',function($scope,Generos){
    $("#title").html('Seleccione un Genero');
    $scope.list = Generos.query();   
    window.plugins.AdMob.showInterstitialAd(true,function(){},function(e){alert(JSON.stringify(e));});
}]);

sonidero.controller('discos',['$scope','$routeParams','$http',function($scope,$routeParams,$http){
    $("#title").html('Seleccione un Disco');
    //$scope.list = Discos.query();
    $(".loading").show();
    $http({
        method: 'POST',
        url: url+'discos',
        data: "id=" + $routeParams.id,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(data){$scope.list = data; $(".loading").hide();});
    window.plugins.AdMob.showInterstitialAd(true,function(){},function(e){alert(JSON.stringify(e));});
}]);

sonidero.controller('disco',['$scope','$routeParams','$http',function($scope,$routeParams,$http){
    $(".loading").show();
    $scope.setAudio = function(url,id){
        musica = $("#musica");
        musica.attr('src',url);
        music = document.getElementById('musica');
        music.play();
        $(".nav i").removeClass('glyphicon-play');
        $("#m"+id).parent('li').find('i').addClass('glyphicon-play');                    
    }
    $scope.download = function(uri,name){                    
        if(confirm('Esta seguro que desea descargar esta cancion para oirla en offline?')){
            var fileTransfer = new FileTransfer();  
            $(".loading").show();
            name = name.replace(/.mp3/g,'');
            name = name.replace(/ /g,"_");
            fileTransfer.download(
                encodeURI(uri),
                "cdvfile://localhost/persistent/proyectosonidero/"+name+".mp3",
                function(entry) {
                    alert("Se ha descargado con éxito el audio para oirlo en modo offline");
                    $(".loading").hide();
                },
                function(error) {
                    $(".loading").hide();
                    alert("download error source " + error.source);
                    alert("download error target " + error.target);
                    alert("upload error code" + error.code);
                }                            
            );
        }
    }
    $http({
        method: 'POST',
        url: url+'disco',
        data: "id=" + $routeParams.id,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(data){
        $(".loading").hide();
        if(data.length>0){
            $scope.data = data; 
            $scope.foto = data[0].foto;
            lista = data;
            $("#title").html(data[0].disco);
        }
        else $("#title").html('Disco no encontrado');
    });
    window.plugins.AdMob.showInterstitialAd(true,function(){},function(e){alert(JSON.stringify(e));});
}]);

sonidero.controller('eventos',['$scope','$http',function($scope,$http){
    $("#title").html('Eventos');
    $(".loading").show();
    $http({
        method:'POST',
        url:url+'eventos',
         headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(data){
        $(".loading").hide();
        $scope.list = data;
    });
    window.plugins.AdMob.showInterstitialAd(true,function(){},function(e){alert(JSON.stringify(e));});
}]);

sonidero.controller('eventosShow',['$scope','$http','$routeParams',function($scope,$http,$routeParams){
    $(".loading").show();
    $http({
        method: 'POST',
        url: url+'eventosShow',
        data: "id=" + $routeParams.id,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).success(function(data){
        $(".loading").hide();
        if(data.titulo!=undefined){
            $scope.data = data;
            $scope.cargarMapa = function(mapa){
                mapa = mapa.replace("(","");
                mapa = mapa.replace(")","");
                mapa = mapa.split(",");
                var mapOptions = {
                zoom: 11,
                center: new google.maps.LatLng(mapa[0],mapa[1]),
                mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById('mapa'),mapOptions);

                var usuario = new google.maps.Marker({
                position: new google.maps.LatLng(mapa[0],mapa[1]),
                map: map,
                title: 'Tu posicion',
                });
            }
            $("#title").html(data.titulo);
        }
        else $("#title").html('Evento no encontrado');
    });
    window.plugins.AdMob.showInterstitialAd(true,function(){},function(e){alert(JSON.stringify(e));});
}]);

sonidero.controller('eventosAdd',['$resource','$scope',function($resource,$scope){
    window.plugins.AdMob.showInterstitialAd(true,function(){},function(e){alert(JSON.stringify(e));});
}]);                                                            

sonidero.controller('music_off',['$resource','$scope',function($resource,$scope){
        $("#title").html('Musica Offline');  
        window.plugins.AdMob.showInterstitialAd(true,function(){},function(e){alert(JSON.stringify(e));});
}]);
//Querys
sonidero.factory('Generos',['$resource',function($resource){
        return $resource(url+'generos',{},{
            query:{method:'GET',params:{},isArray:true}
        });
}]);

sonidero.directive('onLongPress', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $elm, $attrs) {
        $elm.bind('touchstart', function(evt) {
        // Locally scoped variable that will keep track of the long press
        $scope.longPress = true;

        // We'll set a timeout for 600 ms for a long press
        $timeout(function() {
        if ($scope.longPress) {
        // If the touchend event hasn't fired,
        // apply the function given in on the element's on-long-press attribute
        $scope.$apply(function() {
        $scope.$eval($attrs.onLongPress)
        });
        }
        }, 3000);
        });

        $elm.bind('touchend', function(evt) {
        // Prevent the onLongPress event from firing
        $scope.longPress = false;
        // If there is an on-touch-end function attached to this element, apply it
        if ($attrs.onTouchEnd) {
        $scope.$apply(function() {
        $scope.$eval($attrs.onTouchEnd)
        });
        }
        });
        }
    };
});