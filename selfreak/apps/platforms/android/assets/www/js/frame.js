//Libreria javascript Por ing jonathan Cardozo Version 1.0

var fileid = 1;
var files = [];
var obligatorios = [];
var path_lib = 'lib/';

function ajax(data,x,label,callback,url)
{

        if(x==null || x==undefined)x=true;
        else x = false;
        if(url===undefined)url = '';

        if(x)
                try{emergente('Procesando por favor espere...');}catch(e){alert(e.message);}
        else
                $(label).html('Procesando por favor espere...');

        $.ajax({
                        url:url,
                        type:"post",
                        contentType:"application/x-www-form-urlencoded",
                        data:data,
                        success:function(data){
                            if(callback!==undefined)callback(data);

                            else{
                                if(x)
                                try{emergente(data);}catch(e){alert(e.message);}
                                else
                                $(label).html(data);

                                if(data.search('success_message')>0){
                                        emergente('Los datos fueron introducidos con exito');
                                        $("#btn-close").click(function(){document.location.href=window.top.location;});
                                }
                            }
                        }
        });
}

function emergente(data,xs,ys,boton,header){


        var x = (xs==undefined)?(window.innerWidth/2)-325:xs;
        var y = (ys==undefined)?(window.innerHeight/2):ys;
        var b = (boton==undefined || boton)?true:false;
        var h = (header==undefined)?'Mensaje':header;
        if($(".modal").html()==undefined){
        $('body,html').animate({scrollTop: 0}, 800);
        var str = '';
        var str = '<!-- Modal -->'+
        '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
        '<div class="modal-dialog">'+
        '<div class="modal-content">'+
        '<div class="modal-header">'+
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
        '<h4 class="modal-title" id="myModalLabel">'+h+'</h4>'+
        '</div>'+
        '<div class="modal-body">'+
        data+
        '</div>'+
        '<div class="modal-footer">'+
        '<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'+
        '</div>'+
        '</div><!-- /.modal-content -->'+
        '</div><!-- /.modal-dialog -->'+
        '</div><!-- /.modal -->';
        $("body").append(str);
        $("#myModal").modal("toggle");
        }
        else
        {
                $(".modal-body").html(data);
                if($("#myModal").css('display')=='none')
                    $("#myModal").modal("toggle");
        }
}
function val_email(email)
{
        var re  = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/; 
        return !re.test(email)?false:true; 
}

var val = true;
var msj = '';
function validar(form)
{
        val = true;
        $(form).find(':input').each(function(){
                if($(this).attr('data-val')=="required" && $(this).val()=='')
                        {$(this).css({'border-color': '#B94A48'}); val=false;  msj = msj+'El campo <b>'+this.name+'</b> debe ser completado<br/>';}
                if($(this).attr('type')=='email' && !val_email($(this).val()))
                        {$(this).css({'border-color': '#B94A48'}); val=false; msj = msj+'El campo <b>'+this.name+'</b> debe tener un email valido<br/>';}
                if($(this).attr('data-equal')!==undefined && $($(this).attr('data-equal')).val() != $(this).val())
                        {$(this).css({'border-color': '#B94A48'}); $($(this).attr('data-equal')).css({'border-color': '#B94A48'}); val=false; msj = msj+'El campo <b>'+this.name+'</b> debe ser igual a '+$($(this).attr('data-equal')).attr('name')+"<br/>";}
        });
        if(!val){
            $("#"+$(form).attr('data-popup')).html('<p class="alert alert-danger">'+msj+'</p>');
            $("#"+$(form).attr('data-popup')).popup('open');
        }
        return !val?false:true;
}
var post = '';
function send(form,url)
{
        if(validar(form)){
        $(form).find(':input').each(function(){
                post = post+this.name+"="+$(this).val()+"&";
        });
        ajax(post,undefined,undefined,undefined,url);
        }
        return false;
}

function error(msj)
{
    return '<div class="alert alert-danger">'+msj+'</div>';
}
function success(msj)
{
    return '<div class="alert alert-success">'+msj+'</div>';
}

function JSONsend(form,urlValidation,url)
{
    if(validar(form)){
        $(form).find(':input').each(function(){
                post = post+this.name+"="+$(this).val()+"&";
        });
        ajax(post,undefined,undefined,function(data){
            data = data.replace('<textarea>','');
            data = data.replace('</textarea>','');
            var x = JSON.parse(data);
            if(x.success)
            {
                ajax(post,undefined,undefined,function(data){
                data = data.replace('<textarea>','');
                data = data.replace('</textarea>','');
                var x = JSON.parse(data);
                if(x.success)
                emergente(success(x.success_message));
                else
                emergente(error('ocurrio un error en la operacion'));
                },url);
            }
            else
                emergente(error(x.error_message));
        },urlValidation);
    }
    return false;
}

function carousel(fotos)
{
    if(fotos.length==0)fotos.push('img/logo.png');
    str = '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">\n\
            <ol class="carousel-indicators">';
    x = 0;
    for (i in fotos){
    active = x==0?'class="active"':'';
    str+= '<li data-target="#carousel-example-generic" data-slide-to="'+x+'" '+active+'></li>';
    x++;
    }
    str+= '</ol>\n\
            <div class="carousel-inner">';    
    x = 0;
    for (i in fotos){
    active = x==0?'active':'';
    str+= '<div class="item '+active+'">\n\
                <img src="'+fotos[i]+'" style="width:100%; height:200px;">\n\
           </div>';
    x++;
    }
    str+= '</div><a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">\n\
              <span class="glyphicon glyphicon-chevron-left"></span>\n\
            </a>\n\
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">\n\
              <span class="glyphicon glyphicon-chevron-right"></span>\n\
            </a>\n\
          </div>';
    
    return str;
}