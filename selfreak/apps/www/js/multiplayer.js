var url = 'http://217.160.131.136/selfreak/api/'; //URL DEL SERVER
var path = 'http://217.160.131.136/selfreak/';
var token = undefined, id = undefined, nombre = undefined, contrincante = undefined,ws, reloj, reloj_param,jugadores_array,tocarankear=-1,jugador;
var ranking = [0,0,0,0,0],selfie_take_pic;
var maxtimetakeselfie = 120, timetakeselfie = 0;

/*Login */
function login(){
    loading('show');
    if($("#email").val()!='' && $("#user").val()!=''){
        $.post(url+'login',{email:$("#email").val(),'user':$("#user").val()},function(data){            
            if(data.status=='fail'){
                alerta("Usuario o email incorrecto");
            }
            else if(data.status=='noexist'){
                alerta('Su usuario debe registrarse \n\
                <br/><form id="reglogin" onsubmit="return registrar(this)">\n\
                <input type="text" name="nombre" id="nombre" placeholder="Nombre" class="form-control"> <br/>\n\
                <input type="hidden" name="user" value="'+$("#home #user").val()+'"> <input type="hidden" name="email" value="'+$("#home #email").val()+'"> \n\
                <div align="center"><button type="submit" class="btn btn-success">Registrar</button></div>\n\
                </form>','Login de usuario');
            }
            else if(data.status=='blocked'){
                alerta('Usuario <b>Bloqueado</b> comuniquese con un administrador, para m�s informaci�n');
            }
            else{
                document.location.href="#menu";
                loading('close');            
                id = data.id;
                nombre = data.nombre;
                token = data.token;
                ws.login();
                save();
                $("body > header > nav").show();
            }
        });
    }
    else
        alerta("Ingrese un usuario y una contrase�a, Si es un usuario nuevo ingrese el que desea usar para el registro");
    return false;
}

function registrar(form){
    if($("#alerta #nombre").val()!=''){
        loading('show');
        sendData(form,'registrar',function(data){            
            token = data.token;
            id = data.id;
            nombre = data.nombre;
            document.location.href="#menu";
            loading('show');
            refreshLocation();
            save();
            $("body > header > nav").show();
        });
    }
    return false;
}
/* Fin Login */
function initmultiplayer(){    
    alerta('\n\
    <div style="line-height:10px; background:#fff14c; padding:4px; margin:4px 0" class="row">\n\
        <a href="javascript:selmultiplayer(\'user\')" style="color:black;">\n\
        <div class="col-xs-9">Usuario. <br><small style="font-size:8px;">Buscar por nombre</small></div></a>\n\
        <div class="col-xs-2" style="padding:0px" align="center"><img src="img/username.png" style="width:16px"></div>\n\
    </div>\n\
    <div style="line-height:10px; background:#0078bb; color:white; padding:4px; margin:4px 0" class="row">\n\
        <a href="javascript:selmultiplayer(\'email\')" style="color:white;">\n\
        <div class="col-xs-9">Email. <br><small style="font-size:8px;">Buscar por email</small></div></a>\n\
        <div class="col-xs-2" style="padding:0px" align="center"><img src="img/mail.png" style="width:26px"></div>\n\
    </div>\n\
    <div style="line-height:10px; background:#ee0863; color:white; padding:4px; margin:4px 0" class="row">\n\
        <a href="javascript:selmultiplayer(\'aleatorio\')" style="color:white;">\n\
        <div class="col-xs-9">Aleatorio<br/><small style="font-size:8px;">Oponente aleatorio</small></div></a>\n\
        <div class="col-xs-2" style="padding:0px" align="center"><img src="img/random.png" style="width:26px"></div>\n\
    </div>\n\
    ','Crear nueva partida','backtienda');
}

function selmultiplayer(param){    
    switch(param){
        case 'user':
            alerta('\n\
            <div style="line-height:10px; background:#fff14c; padding:4px; margin:4px 0" class="row">\n\
                <div class="col-xs-9">Quien es tu oponente. </div>\n\
                <div class="col-xs-2" style="padding:0px" align="center"><img src="img/username.png" style="width:16px"></div></div>\n\
                <div style="line-height:10px; padding:4px; margin:4px 0" class="row">\n\
                <div class="col-xs-12"><form onsubmit=\'return selmultiplayer2(this)\'><input type="text" name="nombre" class="form-control" placeholder="Nombre"> <br/><div align="center"><button type="submit" style="padding:0px; border:0px"><img src="img/searchbutton.png" style="width:110px"></button></div></form></div>\n\
                </div>',undefined,'backtienda')
                
            //alerta(" ",'Crear nueva partida');
        break;
        case 'email':
            alerta('\n\
            <div style="line-height:10px; color:white; background:#0078bb; padding:4px; margin:4px 0" class="row">\n\
                <div class="col-xs-9">Quien es tu oponente. </div>\n\
                <div class="col-xs-2" style="padding:0px" align="center"><img src="img/mail.png" style="width:26px"></div></div>\n\
                <div style="line-height:10px; padding:4px; margin:4px 0" class="row">\n\
                <div class="col-xs-12"><form onsubmit=\'return selmultiplayer2(this)\'><input type="text" name="email" class="form-control" placeholder="Email"> <br/><div align="center"><button type="submit" style="padding:0px; border:0px"><img src="img/searchbutton.png" style="width:110px"></button></div></form></div>\n\
                </div>',undefined,'backtienda')
            
        break;
        case 'aleatorio':
            alerta('<img src="img/loading.gif" style="width:100%">','Buscando partidas','backtienda');
            ws.initaleatorio();
        break;
    }
}

function selmultiplayer2(form){
   el = $(form).find('input')[0];
   if($(el).val()!=''){
       sendData(form,'init_game',function(data){
           if(data.jugadores.length==0){
               $("#alerta #msj button").before('<span style="color:red">No se han encontrado jugadores conectados</span><br/>');
           }
           else{
               str = '<ul class="list-group">';
               for(i in data.jugadores){
                   str+= '<li class="list-group-item"><a href="javascript:seloponente('+data.jugadores[i].id+')">'+data.jugadores[i].nombre+'</a></li>';
               }
               str+= '</ul>';
               alerta(str,'Selecciona un jugador');
           }
       });
   }
   return false; 
}

function seloponente(id){
   contrincante = id;
   //Crear Sala
   loading('close');   
   ws.send_invitation();
   alerta('Invitación enviada, esperando que el jugador responda <img src="img/loading.gif" style="width:100%">','Esperando respuestas');
}

//Funcion para guardar los datos del usuario en local
function save(){    
    localStorage.setItem("nombre",nombre);
    localStorage.setItem("id",id);
    get_perfil();
}

//Funcion para desconectar datos
function unlog(){
    localStorage.removeItem('nombre');
    localStorage.removeItem('id');    
    document.location.href="#home";
    ws.close();
    connect_to_server();
}

document.addEventListener("deviceready", connect_to_server, false);
document.addEventListener("offline",function(){alerta('No estas conectado a internet por lo tanto no podrás jugar selfreak','Desconectado del servidor')}, false);
$(document).on('ready',function(){connect_to_server();});
//Comienza el juego
function connect_to_server(){
    $("#menu #multiplayer_new #link").attr('href','javascript:initmultiplayer()');
    $("#menu #multiplayer_new #Titulo").html('Nueva Partida');
    $("body > header > nav").hide();        
    if ("WebSocket" in window)
    {       
       ws = new WebSocket("ws://217.160.131.136:3000");       
       ws.onopen = function()
       {
          if(localStorage.id!=undefined)
          {
            //document.location.href = '#menu';
            id = localStorage.id;
            nombre = localStorage.nombre;
            ws.login();
            closealerta();
            refreshLocation();            
            $("body > header > nav").show();
          }
          //ws.send(JSON.stringify({cuenta:'',username:'usernam',userid:'userid',event:'loginuser',data:'id'}));
          //ws.send();  
       };
       ws.onmessage = function (evt) 
       {        
           data = JSON.parse(evt.data);
           switch(data.evt){
               case 'get_invitation':get_invitation(data.data.player,data.data.playername); break; //contrincante recibe invitacion
               case 'answer_invitation': answer_invitation(data.data.respuesta); break; //jugador recibe respuesta de invitacion
               case 'refreshlistplayer': refreshlistplayer(data.data.players); break; //Evento para refrescar lista de jugadores
               case 'wait_for_players': clearTimeout(reloj); reloj_param = data.data.time; wait_for_players(); break;
               case 'wait_for_init_play': clearTimeout(reloj); reloj_param = data.data.time; wait_for_init_play(); break;
               case 'play': clearTimeout(reloj); reloj_param = data.data.time; ronda = data.data.ronda; timetakeselfie = maxtimetakeselfie; closealerta(); play(); break;
               case 'finish_game': clearTimeout(reloj); finish_game(data.data); break;
               case 'msj': alerta(data.data.msj,data.data.title); break;
               case 'changeronda': clean(); alerta('RONDA NUMERO <b>'+ronda+'</b>','Cambio de ronda'); $(".round").html(ronda);
               case 'getranking': fill_resumen(data.data);
           }
           console.log(data);
       };                 
       
       ws.onclose = function()
       {  
           document.location.href="#home";
           clearTimeout(reloj);
           ws.close();
           $("#home").html($("#login").html());
           alerta('\n\
            <div style="line-height:10px; color:white; background:#0078bb; padding:4px; margin:4px 0" class="row">\n\
            <div class="col-xs-6"><h4> Adios</h4> </div>\n\
            <div align="center" style="padding:0px" class="col-xs-6"><img style="width:66px" src="img/goodbye.png"></div></div>\n\
            <div style="line-height:10px; color:white; background:#ee0863; padding:4px; margin:4px 0" class="row">\n\
            <a href="javascript:connect_to_server()" style="color:white"><div class="col-xs-6"><h4> Conectar</h4> </div>\n\
            <div align="center" style="padding:0px" class="col-xs-6"><img style="width:46px" src="img/again.png"></div></div></a>')
       };
       //el jugador invita a un contrincante
       ws.send_invitation = function(){
           ws.send(JSON.stringify({evt:'send_invitation',data:{contrincante:contrincante}}));
       }
       
       //contrincante contesta
       ws.acceptinvitation = function(respuesta){
           ws.send(JSON.stringify({evt:'acceptinvitation',data:{respuesta:respuesta}}))           
       }
       
       ws.login = function(){           
           ws.send(JSON.stringify({evt:'login',data:{id:id,nombre:nombre}}));
       }
       
       ws.closematch = function(){
           ws.send(JSON.stringify({evt:'closematch',data:{}}));
       }
       
       ws.sendPhoto = function(){
           ws.send(JSON.stringify({evt:'sendphoto',data:{}}));
       }       
       
       ws.initaleatorio = function(){
           ws.send(JSON.stringify({evt:'aleatorio',data:{}}));
       }
       
       ws.sendRank = function(){
           ws.send(JSON.stringify({evt:'sendRank',data:ranking}));
       }
       
       ws.change_avatar = function(){
           ws.send(JSON.stringify({evt:'change_avatar',data:{}}));
       }
       
       ws.getmoretime = function(){
           ws.send(JSON.stringify({evt:'getmoretime',data:{}}));
           timetakeselfie+=5;
       }
    }
    else
    {
       // The browser doesn't support WebSocket
       alert("WebSocket NOT supported by your Browser!");
    }
}

//Eventos
function get_invitation(player,playername){
   contrincante = player;
   alerta(playername+" te ha invitado a jugar una partida, aceptas? <a href='javascript:acceptinvitation(true)' class='btn btn-success'>Aceptar</a> <a href='javascript:acceptinvitation(false)' class='btn btn-danger'>Rechazar</a>",'Invitación recibida');
}

//invitacion respondida
function answer_invitation(respuesta){
    if(!respuesta){
        alerta('El contrincante seleccionado no ha aceptado la invitación, intenta con otra persona','Invitación rechazada');
    }
    else{
        init_play();
    }
}
//responder invitacion
function acceptinvitation(respuesta){
    closealerta();
    ws.acceptinvitation(respuesta);
    if(respuesta)
        init_play();
}

//invitaciones aceptadas
function init_play(){    
    document.location.href="#multiplayer";
    closealerta();
    refreshLocation();
    $("#resumen .r").html('0');
}

//Ir llenando el resumen
function fill_resumen(ranking){    
    ranking = ranking.ranking;
    console.log(ranking);
    for(i in ranking){        
        $("#roundresumen_"+ronda+" .resumenrank ."+i).html(ranking[i]);
    }
}
//Refrescar la lista de jugadores
function refreshlistplayer(jugadores){
    str = "<div class='verde list-group-item row' style=\"padding:10px 0;\" align=\"center\"><div class=\"col-xs-9\">Nombre</div><div class=\"col-xs-3\">Rank</div></div>";
    jugadores_array = jugadores;
    for(i in jugadores){
        active = jugadores[i].id==id?'active':'';
        href = jugadores[i].id==id?'javascript:takeselfiemulti()':'javascript:rank_player("'+i+'")';
        //str+= "<li class='list-group-item row "+active+"'><a href='"+href+"'><div class='col-xs-9'>"+jugadores[i].nombre+"</div><div class='col-xs-3'>"+jugadores[i].puntos+"</div></a></li>";
        str+= "<a href='"+href+"' class='list-group-item row "+active+"' style='padding:0px;'>\n\
            <div class=\"col-xs-9\">\n\
                <img src=\""+jugadores[i].foto+"\" style=\"width:30px;\"> "+jugadores[i].nombre+"\n\
            </div>\n\
            <div class=\"col-xs-3\" style=\"padding-top:6px\" align=\"center\">\n\
                "+jugadores[i].puntos+"\n\
            </div>\n\
            </a>";
        
        if(jugadores[i].id==id){
            jugador = jugadores[i];
            $(".monedas").html(jugador.monedas);
            $("#imgavatar").attr('src',path+"/avatars/"+jugadores[i].avatar);
            $("#avatarname").html(jugadores[i].avatarname);
            reloj_param = jugador.time;
        }
        
        if(jugadores[i].id==id && jugadores[i].selfie!=''){
            $("#imgselfie").attr('src',path+jugadores[i].selfie);            
        }
        if(jugadores[i].id==id && jugadores[i].selfie=='') 
            $("#imgselfie").attr('src','img/vacio.png');
        
        if(jugadores[i].id==id && jugadores[i].tocarankear!=-1)
            tocarankear = jugadores[i].tocarankear;
        
        if(tocarankear!=-1)
            rank_player(tocarankear);
    }
    $("#multiplayer #listplayers").html(str);      
}
//Tiempo de espera de jugadores
function wait_for_players(){
    clean();
    reloj_param-= 1;
    alerta('<h1 align="center">'+reloj_param+'</h1>','Esperando por más jugadores');
    if(reloj_param!=0)
    reloj = setTimeout(wait_for_players,1000);
    $("#menu #multiplayer_new #link").attr('href','#multiplayer');
    $("#menu #multiplayer_new #Titulo").html('Partida iniciada');    
    ranking = [0,0,0,0,0];
    clean_stars();
}
//Tiempo para empezar
function wait_for_init_play(){
    reloj_param-=1;
    alerta('<h1 align="center">'+reloj_param+'</h1>','La partida comienza en');
    if(reloj_param!=0)
    reloj = setTimeout(wait_for_init_play,1000);
}
//Comeinza el juego
function play(){
    reloj_param+=1; 
    if(timetakeselfie>0){
        timetakeselfie-=1;
        $(".timetakeselfie").html(timetakeselfie);        
        if($("#imgselfie").attr('src')=='img/vacio.png' && get_action()!='take_selfie')
        takeselfiemulti();
    }
    else if(timetakeselfie==0 && $("#imgselfie").attr('src')=='img/vacio.png'){
        $("#imgselfie").attr('src','img/selfie.png');
        sendphoto();
    }
    if(tocarankear!=-1 && get_action()!='rank_player'){
        rank_player(tocarankear);  
    }
    $("#multiplayer #reloj").html(reloj_param);
    $("#ronda").html(ronda);
    if(reloj_param!=0)
    reloj = setTimeout(play,1000);
}
//Juego termina+
function finish_game(data){
    clearTimeout(reloj);    
    $("#menu #multiplayer_new #link").attr('href','javascript:initmultiplayer()');
    $("#menu #multiplayer_new #Titulo").html('Nueva Partida');
    if(jugador!=undefined && jugador.ranking_group!=undefined && jugador.ranking_group.length>0){
        //Setear resumen
        $("#finish_multiplayer #finish_name").html(nombre);
        whowinim = jugador.ranking_group[0].id!=id?'loser.png':'winner.png';
        $("#whowinimg").attr('src','img/'+whowinim);
        $("#finish_avatar img").attr('src',$("#imgavatar").attr('src'));
        $("#finish_selfie img").attr('src',path+jugador.selfie);
        $("#finish_seconds").html(reloj_param);
        $("#finish_points").html(jugador.puntos);
        $("#woncoins").html(jugador.puntos);
        str = '<div class="verde list-group-item row" style="padding:10px 0;" align="center"><div class="col-xs-9">Nombre</div><div class="col-xs-3">Rank</div></div>';
        for(i in jugador.ranking_group){
            active = jugador.ranking_group[i].id==id?'active':'';
            str+= "<div class='list-group-item row "+active+"' style='padding:0px;'>\n\
                <div class=\"col-xs-9\">\n\
                    <img src=\""+jugador.ranking_group[i].foto+"\" style=\"width:30px;\"> "+jugador.ranking_group[i].nombre+"\n\
                </div>\n\
                <div class=\"col-xs-3\" style=\"padding-top:6px\" align=\"center\">\n\
                    "+jugador.ranking_group[i].puntos+"\n\
                </div>\n\
                </div>";
        }
        $("#listwinplayers").html(str);    
        //Pintar ranking final
        ranking = jugador.ranking;
        for(i in ranking){
            paint_stars($("#finish_rank").find('.rate')[i],ranking[i]);
        }    
        $("#listwinplayers").show();
        $("#takeotherselfie").hide();
        if(whowinim=='winner.png'){
            $("#animationcoin").addClass('monedawin');
            iwin();
        }
        else
        document.location.href="#finish_multiplayer";
    }
    else{
        document.location.href="#menu";
        alerta(data.msj);
    }
    refreshLocation();
}
function iwin(){
    document.location.href="#woncash";
    setTimeout(function(){
        $("#animationcoin").removeClass('monedawin');
        document.location.href="#finish_multiplayer"
    },3000);
}

//SI el jugador desea cerrar la partida
function out_of_game(){
    if(confirm('¿Seguro que deseas salir?')){
        ws.closematch();
        finish_game({msj:'Haz abandonado la partida'});
    }
}

//Funcion para tomar la camara y capturar foto
function takefoto(){
    //Funcion para testear desde la pc
    //$("#selfie img").attr('src','img/selfie.png');
    //Funcion para la camara de android comentar anterior
    navigator.camera.getPicture(function(imageData){
        img = document.getElementById('imgselfie');
        img.src = "data:image/jpeg;base64,"+imageData;
        selfie_take_pic = imageData;
    },function(){finish_game(); alerta('ERROR EN CONEXION CON LA CAMARA','ERROR')}, { quality: 45,targetWidth: 284, targetHeight: 284, allowEdit: true, destinationType: navigator.camera.DestinationType.DATA_URL });
}

//Transformar la foto para enviarla
function getBase64Image(img) {
  // Create an empty canvas element
  var canvas = document.createElement("canvas");
  canvas.width = 284;
  canvas.height = 284;  
  // Copy the image contents to the canvas
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0,canvas.width,canvas.height);
 
  // Get the data-URL formatted image
  // Firefox supports PNG and JPEG. You could check img.src to guess the
  // original format, but be aware the using "image/jpg" will re-encode the image.
  var dataURL = canvas.toDataURL("image/png");
  return dataURL;
}

//Funcion para enviar la foto tomada
function sendphoto(){
    if(timetakeselfie>0){
        if(confirm('¿Seguro que deseas enviar esta foto?')){
            sendPhotonow();
        }
    }
    else sendPhotonow();
}

function sendPhotonow(){
    var img = getBase64Image(document.getElementById("imgselfie"));
    //img = selfie_take_pic;
    alerta('<img src="img/loading.gif" style="width:100%">','Enviando selfie');
    $("#takephotobutton, #sendphotobutton").hide();
    $.post(url+"uploadSelfie",{img:img,id:id},function(data){
        ws.sendPhoto();
        alerta("Selfie enviada");
    });
}
//Cargar la interfaz de selfie
function takeselfiemulti(){
    closealerta();
    document.location.href="#take_selfie";
    $("#sendphotobutton a").attr('href','javascript:sendphoto()');
    $("#changeavatar").attr('href','javascript:change_avatar()');
    $(".timetakeselfie").show();
}

//Cargar interfaz del jugador seleccionado para rankear
function rank_player(id){
    clean_stars();
    contrincante = id;
    if(id==tocarankear)$("#sendranking").show();
    else $("#sendranking").hide();
    datos = jugadores_array[id];
    selfie = path+jugadores_array[id].selfie;
    selfie = jugadores_array[id].selfie==''?'img/vacio.png':selfie;
    $("#rankselfie").attr('src',selfie);
    $("#rankavatar").attr('src',path+"/avatars/"+jugadores_array[id].avatar)
    $("#ranktime").html(jugadores_array[id].time);
    $(".avatarname").html(jugadores_array[id].avatarname);
    $(".rankname").html(jugadores_array[id].nombre);
    $("#sendrankButton").show();
    $("#sendrankButton").attr('href','javascript:sendRank()');
    $("#ranktimediv").show();
    closealerta();
    document.location.href="#rank_player";
    console.log(jugadores_array[id]);
}

function clean()
{
    $("#takephotobutton, #sendphotobutton").show();
    $("#rankselfie").attr('src','img/vacio.png');
    $("#imgselfie").attr('src','img/vacio.png');
    $("#ronda").html('');
    $("#reloj").html('');
    clean_stars();
    contrincante = 0;
    tocarankear = -1;
    ranking = [0,0,0,0,0];
}

function sendRank(){
    v = 0;
    for(i in ranking)
    {
        if(ranking[i]!=0)
            v++;
    }
    if(v==ranking.length){
        ws.sendRank();
        document.location.href="#multiplayer";
        tocarankear = -1;
        document.location.href="#publicidad";
    }
    else alert('Califique todos los atributos de este jugador');
}

function paint_stars(ul,val){
    $(ul).find('li').removeClass('act');
    for(i = 0; i<val; i++)
      $($(ul).find('li')[i]).addClass('act');   
}

function clean_stars(){
    $(".rate li").each(function(){$(this).removeClass('act')})
}

function change_avatar(){
    ws.change_avatar();
}

//Cuando se pulse en el ranking
$(document).on('click',".rate li",function(evt){    
    ul = $(this).parent('ul');
    val = $(this).index()+1;
    paint_stars(ul,val);
    ranking[$(".rate").index(ul)] = val;
})

function more_time(){
    if(confirm('Seguro que desea gastar 5 monedas para obtener mas tiempo?'))
    ws.getmoretime();
}
