$(document).on('ready',function(){
    refreshLocation(true);
    //Append alerta
    str = '<div id="alerta" class="alertainvisible backmulti">\n\
            <nav class="row">\n\
                <div class="col-xs-10">Alert</div>\n\
                <a href="javascript:closealerta()"><div class="col-xs-2" style="border: 1px solid white; border-radius: 1em; padding: 1px 5px; text-align:center"><i class="glyphicon glyphicon-remove"></i> </div></a>\n\
            </nav>\n\
            <div id="msj">\n\
                \n\
            </div>\n\
        </div>';
    $('body').append(str);
    resize();
 });
 
 $(window).on('resize',resize);
 
 $(document).on('click','a',function(e){
     e.preventDefault();
     mostrar($(this).attr('href'));                
     document.location.href=$(this).attr('href');
 });
 
 $(document).on('click','body',function(e){
     if($("aside").hasClass('pagecenter'))menu();
 });
 
 function resize(){
     $('body > header > aside > article').css('height',(parseInt(window.innerHeight)-parseInt($('body > header').height()))+"px");
 }

 function mostrar(id){
     $(id).removeClass('pageleft').addClass('pagecenter');
     $(document).scrollTop(0);
 }
 
 function get_action(){
    activePage = document.location.href;
    activePage = activePage.split('#');
    if(activePage.length>1)
        return activePage[1];
    else return 'home';
 }

 function refreshLocation(refresh){    
    activePage = document.location.href;
    activePage = activePage.split('#');
    $("body > section > article").removeClass('pagecenter').addClass('pageleft');
    if(activePage.length>1)
    {
        mostrar("#"+activePage[1]);        
    }
    else {
        $("body > section > article").removeClass('pagecenter').addClass('pageleft');
        $("body > section > #home").addClass('pagecenter').removeClass('pageleft');
    }
    
    if(refresh){        
        $("body > section > article").each(function(){           
            if($(this).data('async')!=undefined)
            $("#"+$(this).attr('id')).load($(this).data('async'));
        })
    }
 }
 
 //Funcion para mostrar un mensaje emergente
 function alerta(msj,title,action){
    action = action==undefined?'backmulti':'backtienda';
    $("#alerta nav .col-xs-10").html(title);
    $("#alerta #msj").html(msj);    
    $("#alerta").removeClass('backmulti');
    $("#alerta").removeClass('backtienda');
    $("#alerta").addClass(action);
    $("#alerta").addClass('alertavisible').removeClass('alertainvisible');    
}

function closealerta(){
    $("#alerta").addClass('alertainvisible').removeClass('alertavisible');
}

//Funcion para mostrar el loading emergente
function loading(param){
    if(param=='show'){
        $("#alerta nav .col-xs-10").html('Cargando...');
        $("#alerta #msj").html('<img src="img/loading.gif" style="width:100%;">');
        $("#alerta").addClass('alertavisible').removeClass('alertainvisible');
    }
    else
        $("#alerta").removeClass('alertavisible').addClass('alertainvisible');
}

//Funcion para mostrar el aside o menu lateral
function menu(param){
    if($("aside").hasClass('pageleft'))
    $("aside").removeClass('pageleft').addClass('pagecenter');
    else
    $("aside").removeClass('pagecenter').addClass('pageleft');
}
//Funcion para enviar datos al servidor
function sendData(form,uri,onsuccess,onfail){   
    formData = new FormData(form);
    $.ajax({
        url: url+uri,
        data: formData,            
        cache: false,
        contentType: false,
        processData: false,
        method:'post',
        success:function(data){            
            if(data.status=='success')
                onsuccess(data);
            else 
                onfail(data);
        }
   });
}