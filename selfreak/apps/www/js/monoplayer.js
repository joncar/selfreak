var monoavatar = '',imgmono = '', selfid = '';
function takeselfiemono(){
    //alerta('<img src="img/loading.gif" style="width:100%">','Estamos preparando el selfie');
    $.post(url+"getsalfiemono",{id:id},function(data){
        if(data.status=='success'){
            //closealerta();
            document.location.href="#finish_multiplayer";
            $("#who_win, #finish_name").html('');
            $("#finish_selfie img").attr('src',path+"pictures_alone/"+id+".png");
            $("#finish_avatar img").attr('src',path+"/avatars/"+monoavatar);
            $("#finish_seconds").html('');
            $("#finish_points").html(data.datos.puntos);
            $("#listwinplayers").hide();
            $("#takeotherselfie,#takephotobutton,#sendphotobutton").show();
            //Pintar ranking
            paint_stars($("#finish_rank").find('.rate')[0],data.datos.ojos);
            paint_stars($("#finish_rank").find('.rate')[1],data.datos.cabello);
            paint_stars($("#finish_rank").find('.rate')[2],data.datos.nariz);
            paint_stars($("#finish_rank").find('.rate')[3],data.datos.boca);
            paint_stars($("#finish_rank").find('.rate')[4],data.datos.atuendo);                        
        }
        else
        {
            document.location.href="#take_selfie";
            $("#imgavatar").attr('src',path+"/avatars/"+data.avatar);
            $("#imgselfie").attr('src','img/vacio.png');
            $(".avatarname").html(data.avatarname);
            monoavatar = data.avatar;
            $("#sendphotobutton a").attr('href','javascript:sendphotomono()');
            $("#changeavatar").attr('href','javascript:change_avatar_mono()');
            $("#takephotobutton, #sendphotobutton").show();
            $(".timetakeselfie").hide();
            clean_stars();
        }
        //closealerta();
    });    
}
//Funcion para enviar la foto tomada
function sendphotomono(){
    if(confirm('¿Seguro que deseas enviar esta foto?')){
        var img = getBase64Image(document.getElementById("imgselfie"));
        imgmono = img;
        //img = selfie_take_pic;
        alerta('<img src="img/loading.gif" style="width:100%">','Enviando selfie');
        $("#takephotobutton, #sendphotobutton").hide();
        $.post(url+"uploadSelfieMono",{img:img,id:id,avatar:monoavatar},function(data){
            alerta("Selfie enviada, cuando un jugador rankee su selfie le indicaremos");
            takeselfiemono();
            //finish_monoplayer();
        });
        
    }
}

function finish_monoplayer(){
    $("#finish_avatar img").attr('src',path+"/avatars/"+monoavatar);
    $("#finish_selfie img").attr('src',imgmono);
    document.location.href="#finish_multiplayer";
}

//Funcio para descartar el selfie
function takeotherselfie(){
    alerta('<img src="img/loading.gif" style="width:100%">','Estamos Borrando su selfie anterior');
    $.post(url+"takeotherselfie",{id:id},function(data){
        if(data.status=='success'){
            takeselfiemono();
            closealerta();
        }
        else
        {
            alerta('ocurrio un error');
        }
    });    
}

//Funcion para rankear el selfie
function rankselfiemono(){
    alerta('<img src="img/loading.gif" style="width:100%">','Estamos buscando un jugador');
    $.post(url+"rankselfiemono",{id:id},function(data){
        if(data.status=='success'){
            $("#rankselfie").attr('src',path+"pictures_alone/"+data.datos.selfie);
            $("#rankavatar").attr('src',path+"avatars/"+data.datos.avatar);
            $(".rankname").html(data.datos.nombre);
            $("#sendrankButton").attr('href','javascript:sendRankMono('+data.datos.jugador+')');
            $("#ranktimediv").hide();
            $("#sendrankButton").show();
            $(".avatarname").html(data.datos.avatarname);
            selfid = data.datos.id;
            closealerta();
            clean_stars();
            document.location.href="#rank_player";            
        }
        else
        {
            document.location.href="#menu";
            alerta("Lo sentimos no hemos encontrado selfies para rankear","Selfies no encontrados");
            
        }
    }); 
}

//Funcion para enviar el ranking
function sendRankMono(player){
    v = 0;
    for(i in ranking)
    {
        if(ranking[i]!=0)
            v++;
    }    
    if(v==ranking.length){                
        alerta('<img src="img/loading.gif" style="width:100%">','Estamos enviando su ranking');
        $("#sendrankButton").hide();
        $.post(url+'setRankMono',{jugador:player,rankeador:id,ranking:ranking,id:selfid},function(data){
            if(data.status=='success'){
                alerta('Haz rankeado al jugador','Ranking enviado');
                rankselfiemono();
                get_perfil();
                ranking = [];
            }
        })
    }
    else alerta('Califique todos los atributos de este jugador','error');
}

function change_avatar_mono(){
    alerta('<img src="img/loading.gif" style="width:100%">','Solicitando otro avatar');
    $.post(url+'restpoints',{cantidad:5,jugador:id},function(data){
        if(data.status=='success'){
            get_perfil();
            takeselfiemono();
            closealerta();
        }
        else alerta('No tienes monedas suficientes para realizar esta acción');
    })
}
